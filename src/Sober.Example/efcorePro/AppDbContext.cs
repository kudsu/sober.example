﻿using System.Reflection.Emit;
using System.Reflection;
using Microsoft.EntityFrameworkCore;

namespace efcorePro
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // 获取所有继承自 Entity 类的类型
            var entityTypes = Assembly.GetExecutingAssembly()
                                      .GetTypes()
                                      .Where(t => !t.IsAbstract && t.IsSubclassOf(typeof(AppEntity)));

            foreach (var entityType in entityTypes)
            {
                // 使用 Set 方法根据类型动态注册 DbSet
                modelBuilder.Entity(entityType);
            }
        }
    }
    public abstract class AppEntity
    {
        // 如果有一些通用的属性或方法，可以在这里定义
    }
}
