﻿using Microsoft.AspNetCore.Mvc;

namespace efcorePro.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class HomeController : ControllerBase
    {
        private readonly IConfiguration _conf;

        public HomeController(IConfiguration conf)
        {
            _conf = conf;
        }
        [HttpGet]
        public IActionResult Index()
        {
            string env = "";
            if (_conf["LocalOrServer"] == "server")
            {
                env = "正式环境";
            }
            else if (_conf["LocalOrServer"] == "local")
            {
                env = "本地环境";
            }
            else
            {
                env = "为识别到当前环境";
            }

            return Content(env);

        }
        public class ProductDTO
        {
            public string? Name { get; set; }
            public string Description { get; set; } = "";
            public string Url { get; set; } = "";
            public int Sex { get; set; } = -1;
        }
        public IActionResult Test(ProductDTO model)
        {
            return new JsonResult("");
        }
    }
}
