﻿using System.ComponentModel.DataAnnotations.Schema;

namespace efcorePro.Models
{
    public class SPActivitySub : AppEntity
    {
        public long Id { get; set; }
        public long SPActivityId { get; set; }
        /// <summary>
        /// 活动项目Id
        /// </summary>
        public long SPActivityProjectId { get; set; }
        public long SPActivitySub_ExtendId { get; set; }

        public string Cover { get; set; } //封面/海报
        /// <summary>
        /// 活动预热海报
        /// </summary>
        public string EventWarmUpPoster { get; set; }
        public string Title { get; set; }
        public int CouponValue { get; set; }  //券面值

        public int EventType { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }

        /// <summary>
        /// 是否限定范围
        /// </summary>
        public int WhetherLimitRange { get; set; }

        /// <summary>
        /// 是否限定产品
        /// </summary>
        public int WhetherLimitProduct { get; set; }

        /// <summary>
        /// 是否限制出库经销商
        /// </summary>
        public int WhetherLimitOutputStoreRtmDealer { get; set; }

        public byte EnabledState { get; set; }

        /// <summary>
        /// 是否计算
        /// </summary>
        public byte WhetherCalc { get; set; }

        /// <summary>
        /// 奖励计算方式
        /// </summary>
        public byte RewardCalcType { get; set; }


        /// <summary>
        /// 奖励方式
        /// </summary>
        public byte RewardMethod { get; set; }

        /// <summary>
        /// 奖励上限类型
        /// </summary>
        public byte RewardLimitType { get; set; }

        /// <summary>
        /// 品类数门槛
        /// </summary>
        public int ThresholdCateGoryCount { get; set; }

        /// <summary>
        /// 增长率门槛
        /// </summary>
        public decimal ThresholdGrowthRate { get; set; }

        /// <summary>
        /// 活动编码池子ID
        /// </summary>
        public long SPActivityCodePoolId { get; set; }


        /// <summary>
        /// 满足组合数
        /// </summary>
        public int MeetCount { get; set; }

        /// <summary>
        /// 阶梯类型
        /// </summary>
        public byte TieredType { get; set; }

        public DateTime CreateTime { get; set; }
    }
}
