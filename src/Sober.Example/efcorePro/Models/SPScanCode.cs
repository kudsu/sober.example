﻿namespace efcorePro.Models
{
    public class SPScanCode : AppEntity
    {
        public long Id { get; set; }
        public int ShopkeeperId { get; set; }  //零售商Id：DowShopkeeper表Id
        public string ShopkeeperName { get; set; } //零售商名称
        public string Telephone { get; set; }  //零售商手机号
        public string BoxCode { get; set; } //箱码
        public string ProductCode { get; set; } //产品代码 
        public int Status { get; set; }   //同步状态  1：未同步进箱码入库表  2：已同步 3：已重复
        public long Integral { get; set; } //记录该箱积分  /记录该箱原始积分  未加倍

        public byte IntegralStatus { get; set; } //积分状态 1：未达PO，2：已达PO，3：达PO奖励
        public DateTime ScanDate { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }

        public byte SPActivityRewardGiven { get; set; } //是否已发奖

        //物流顺序号
        public string BoxSerialNO { get; set; }
        //记录当时所扫的码（兼容谷欢编码时加的，主要为了记录扫的谷欢瓶码）
        public string ScanCode { get; set; }

        public string Remark { get; set; }

        public byte CrossAreaState { get; set; }

        /// <summary>
        /// 箱重量
        /// </summary>
        public decimal BoxWeight { get; set; }

        //箱价格
        public decimal BoxPrice { get; set; }
        /// <summary>
        /// ECC价格
        /// </summary>
        public decimal BoxPriceECC { get; set; }
    }
}
