﻿using System.ComponentModel.DataAnnotations.Schema;

namespace efcorePro.Models
{
    public class SPShopkeeper : AppEntity
    {
        public int ShopkeeperId { get; set; }
        public string ShopkeeperCode { get; set; }
        //店铺名称
        public string ShopkeeperName { get; set; }
        public int Type { get; set; } //零售类型  1-零售商 2-县级  4-DG  枚举：SKTypeEnum
        public string Area { get; set; }
        public string Password { get; set; }
        //零售商名称
        public string ContactMan { get; set; }
        public string Telephone { get; set; }
        public string Province { get; set; }
        public string City { get; set; }
        public string County { get; set; }
        public string Address { get; set; }
        public decimal CropAcreage { get; set; }

        public decimal Longitude { get; set; }

        public decimal Latitude { get; set; }
        public string location { get; set; }
        public string Remark { get; set; }
        public string InsectTag { get; set; }
        public string OpenId { get; set; }
        public string Zx_OpenId { get; set; }
        public string UnionId { get; set; }
        //种植精英荟的销售
        /// <summary>
        /// 种植精英荟的销售
        /// </summary>
        public int SalesmanID { get; set; }
        //星伙伴负责人ID(销售Id)
        /// <summary>
        /// 星伙伴负责人ID(销售Id)
        /// </summary>
        public int SPSalesmanID { get; set; }
        public string WXProvince { get; set; }
        public string WXCity { get; set; }
        public int Status { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public string IP { get; set; }
        public string IP_Location { get; set; }
        public string IP_Province { get; set; }
        public string IP_City { get; set; }
        public string Wx_Name { get; set; }
        public string Wx_Img { get; set; }
        //登录状态，0下线  1在线
        public byte LoginStatus { get; set; }
        public string Email { get; set; }
        //SFA编码
        public string SFACode { get; set; }


        public string CRMKey { get; set; }

        //【2021-01-05】星伙伴项目新加字段
        //微信性别
        public string Wx_Sex { get; set; }
        //登录设备
        public string Device { get; set; }
        //最后一次登录时间
        public DateTime LastLoginDate { get; set; }
        //出生日期
        public DateTime Birthday { get; set; }
        //营业执照号
        public string LicenseNo { get; set; }
        //MDM省Key
        public string ProvinceKey { get; set; }
        //MDM市Key
        public string CityKey { get; set; }
        //MDM区Key
        public string CountyKey { get; set; }
        //MDM_District表Id
        public int MDMDistrictId { get; set; }
        //累计积分（用来计算星伙伴等级）
        public long TotalPoints { get; set; }
        //当前总积分（用来做积分兑换）
        public long CurrentPoints { get; set; }

        //星伙伴等级ID
        public int SPLevelId { get; set; }

        //是否已做升级提醒 1已提醒  2未提醒 默认1
        public byte SPUpgradeRemind { get; set; }

        //管家模块数据展示状态  1展示 0不展示 枚举：YesOrNo
        public byte BMShowStatus { get; set; }
        /// <summary>
        /// 商城小程序路径
        /// </summary>
        public string YouzanShopLink { get; set; }
        public int ParentSalesID { get; set; }
        public string Medals { get; set; }
    }
}
